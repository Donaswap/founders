![Stars](https://img.shields.io/gitlab/stars/donaswap/founders?style=social) 
![Contributors](https://img.shields.io/gitlab/contributors/Donaswap/founders)
![Discord](https://img.shields.io/discord/851473572772970527?label=Discord)
![Twitter Follow](https://img.shields.io/twitter/follow/donaswap?style=social)
![Reddit](https://img.shields.io/reddit/subreddit-subscribers/donaswap?style=social)
![YouTube](https://img.shields.io/youtube/channel/subscribers/UC8VT6aehejnROEApR2xEgIw?style=social)
![Last Commit](https://img.shields.io/gitlab/last-commit/Donaswap/founders)

<div align="center">
  <img src="resources/founders.png" height="400" width="400">
</div>

# **FOUNDERS INFORMATION**

## Messiah - Kryptonacci 
<img src="resources/kryptonacci.jpg" height="200">

```

{
  "Title": "Founder",
  "Name": "Kryptonacci",
  "age": "Unknown",
  "Description": "The anonymous founder of the Fire-themed Ecosystem is known as Kryptonacci. However, very little is known of the mystery founder of the Fire-themed cryptocurrency, much like the founder of Bitcoin, Satoshi Nakamoto and the founder of Shiba Inu, Ryoshi."
}
```
![Twitter Follow](https://img.shields.io/twitter/follow/kryptonacci?style=social)

---

## Gino "Kryptstein" Giovanni
<img src="resources/kryptstein.jpg" height="200">

```
{
  "Title": "Founder",
  "Name": "Kryptstein",
  "age": "37",
  "Description": "Since 2021, Kryptstein has been actively involved with blockchain technology and cryptocurrencies. In 2021, Kryptonacci & Kryptstein officially launched DONASWAP, and he has been the CEO of the company ever since."
}
```
![Twitter Follow](https://img.shields.io/twitter/follow/mrdongg?style=social)